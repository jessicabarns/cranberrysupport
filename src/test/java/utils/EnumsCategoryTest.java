package utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class EnumsCategoryTest {

	@Test
	public void testGetCategoryFromString() throws Exception{
		String webService = Enums.Category.WEB_SERVICE.getValue();
		String userAccount = Enums.Category.USER_ACCOUNT.getValue();
		String other = Enums.Category.OTHER.getValue();
		
		assertTrue(Enums.Category.getCategoryFromString(webService).equals(Enums.Category.WEB_SERVICE));
		assertTrue(Enums.Category.getCategoryFromString(userAccount).equals(Enums.Category.USER_ACCOUNT));
		assertTrue(Enums.Category.getCategoryFromString(other).equals(Enums.Category.OTHER));
	}
	
	@Test (expected = Exception.class)
	public void testGetCategoryFromStringException() throws Exception{
		Enums.Category.getCategoryFromString("hello");
	}
}
