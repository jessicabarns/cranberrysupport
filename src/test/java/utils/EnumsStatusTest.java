package utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class EnumsStatusTest {
	
	@Test
	public void testIsFinal() throws Exception{
		assertTrue(Enums.Status.SUCCESS.isFinal());
		assertTrue(Enums.Status.DROPPED.isFinal());
	}
	
	@Test
	public void testIsNotFinal(){
		assertFalse(Enums.Status.OPEN.isFinal());
		assertFalse(Enums.Status.ONGOING.isFinal());
	}
	
	@Test
	public void testGetStatusFromString() throws Exception{
		String dropped = Enums.Status.DROPPED.getValue();
		String success = Enums.Status.SUCCESS.getValue();
		
		assertTrue(Enums.Status.getStatusFromString(dropped).equals(Enums.Status.DROPPED));
		assertTrue(Enums.Status.getStatusFromString(success).equals(Enums.Status.SUCCESS));
	}
	
	@Test (expected = Exception.class)
	public void testGetStatusFromStringException() throws Exception{
		Enums.Status.getStatusFromString("hello");
	}
}
