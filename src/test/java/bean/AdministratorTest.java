package bean;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AdministratorTest {
	Administrator admin;
	
	@Mock
	Technician technician;

	@Before
	public void setUp() throws Exception {
		admin = new Administrator("Jessica", "Barns", "jessicabarns", "1234");
	}

	@Test
	public void testSameAs() {
		assertTrue(admin.sameAs(clone(admin)));
		
		technicianClone(admin);
		assertFalse(admin.sameAs(technician));
		
		Administrator different = clone(admin);
		
		different.setUsername("different");
		assertFalse(admin.sameAs(different));
		
		different = clone(admin);
		different.setFirstname("different");
		assertFalse(admin.sameAs(different));
		
		different = clone(admin);
		different.setLastname("different");
		assertFalse(admin.sameAs(different));
		
		different = clone(admin);
		different.setPassword("different");
		assertFalse(admin.sameAs(different));
		
		different = clone(admin);
		different.setEmail("different");
		assertFalse(admin.sameAs(different));
		
		different = clone(admin);
		different.setPhone("different");
		assertFalse(admin.sameAs(different));
		
		different = clone(admin);
		different.setOffice("different");
		assertFalse(admin.sameAs(different));
	}

	private Administrator clone(Administrator admin) {
		Administrator clone = new Administrator(admin.getFirstname(), admin.getLastname(), admin.getUsername(), admin.getPassword());
		clone.setPhone(admin.getPhone());
		clone.setEmail(admin.getEmail());
		clone.setOffice(admin.getOffice());
		return clone;
	}
	
	private void technicianClone(Administrator admin) {
		Mockito.when(technician.getFirstname()).thenReturn(admin.getFirstname());
		Mockito.when(technician.getLastname()).thenReturn(admin.getLastname());
		Mockito.when(technician.getUsername()).thenReturn(admin.getUsername());
		Mockito.when(technician.getPassword()).thenReturn(admin.getPassword());
		Mockito.when(technician.getPhone()).thenReturn(admin.getPhone());
		Mockito.when(technician.getEmail()).thenReturn(admin.getEmail());
		Mockito.when(technician.getOffice()).thenReturn(admin.getOffice());
	}
}
