package bean;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import utils.Enums;

@RunWith(MockitoJUnitRunner.class)
public class TechnicianTest {
	Technician technician;

	@Mock
	Request request;
	@Mock
	Client client;

	@Before
	public void setUp() throws Exception {
		technician = new Technician("Jessica", "Barns", "jessicabarns", "1234");
		Mockito.when(request.getStatus()).thenReturn(Enums.Status.ONGOING);
		Mockito.when(request.getAssignee()).thenReturn(technician);
	}

	@Test
	public void testAssignRequest() throws Exception {
		technician.assignRequest(request);
		assertTrue(technician.getRequests().contains(request));
	}

	@Test
	public void testSameAs() {
		assertTrue(technician.sameAs(clone(technician)));	
		
		clientClone(technician);
		assertFalse(technician.sameAs(client));

		Technician different = clone(technician);

		different.setUsername("different");
		assertFalse(technician.sameAs(different));

		different = clone(technician);
		different.setFirstname("different");
		assertFalse(technician.sameAs(different));

		different = clone(technician);
		different.setLastname("different");
		assertFalse(technician.sameAs(different));

		different = clone(technician);
		different.setPassword("different");
		assertFalse(technician.sameAs(different));

		different = clone(technician);
		different.setEmail("different");
		assertFalse(technician.sameAs(different));

		different = clone(technician);
		different.setPhone("different");
		assertFalse(technician.sameAs(different));

		different = clone(technician);
		different.setOffice("different");
		assertFalse(technician.sameAs(different));
	}

	public void addRequests(int numberOfRequests) throws Exception {
		for (int i = 0; i < numberOfRequests; i++) {
			technician.assignRequest(request);
		}
	}

	public Technician clone(Technician technician) {
		Technician clone = new Technician(technician.getFirstname(), technician.getLastname(), technician.getUsername(),
				technician.getPassword());
		clone.setEmail(technician.getEmail());
		clone.setOffice(technician.getOffice());
		clone.setPhone(technician.getPhone());
		return clone;
	}
	
	public void clientClone(Technician technician){
		Mockito.when(client.getEmail()).thenReturn(technician.getEmail());
		Mockito.when(client.getFirstname()).thenReturn(technician.getFirstname());
		Mockito.when(client.getLastname()).thenReturn(technician.getLastname());
		Mockito.when(client.getOffice()).thenReturn(technician.getOffice());
		Mockito.when(client.getPassword()).thenReturn(technician.getPassword());
		Mockito.when(client.getPhone()).thenReturn(technician.getPhone());
		Mockito.when(client.getUsername()).thenReturn(technician.getUsername());
	}
}
