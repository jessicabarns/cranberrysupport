package bean;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ClientTest {
	Client client;
	
	@Mock
	Technician technician;
	
	@Before
	public void setUp() throws Exception {
		client = new Client("Jessica", "Barns", "jessicabarns", "1234");
	}

	@Test
	public void testSameAs() {
		assertTrue(client.sameAs(clone(client)));
		
		technicianClone(client);
		assertFalse(client.sameAs(technician));
		
		Client different = clone(client);
		
		different.setUsername("different");
		assertFalse(client.sameAs(different));
		
		different = clone(client);
		different.setFirstname("different");
		assertFalse(client.sameAs(different));
		
		different = clone(client);
		different.setLastname("different");
		assertFalse(client.sameAs(different));
		
		different = clone(client);
		different.setPassword("different");
		assertFalse(client.sameAs(different));
		
		different = clone(client);
		different.setEmail("different");
		assertFalse(client.sameAs(different));
		
		different = clone(client);
		different.setPhone("different");
		assertFalse(client.sameAs(different));
		
		different = clone(client);
		different.setOffice("different");
		assertFalse(client.sameAs(different));
	}
	
	public Client clone(Client client){
		Client clone = new Client(client.getFirstname(), client.getLastname(), client.getUsername(), client.getPassword());
		clone.setEmail(client.getEmail());
		clone.setOffice(client.getOffice());
		clone.setPhone(client.getPhone());	
		return clone;
	}
	
	public void technicianClone(Client client){
		Mockito.when(technician.getFirstname()).thenReturn(client.getFirstname());
		Mockito.when(technician.getLastname()).thenReturn(client.getLastname());
		Mockito.when(technician.getUsername()).thenReturn(client.getUsername());
		Mockito.when(technician.getPassword()).thenReturn(client.getPassword());
		Mockito.when(technician.getPhone()).thenReturn(client.getPhone());
		Mockito.when(technician.getEmail()).thenReturn(client.getEmail());
		Mockito.when(technician.getOffice()).thenReturn(client.getOffice());
	}
}
