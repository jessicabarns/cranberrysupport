package bean;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import dao.RequestDAO;
import exceptions.DatabaseException;
import utils.Enums;

@RunWith(MockitoJUnitRunner.class)
public class RequestTest {
	Request request;
	
	@Mock
	Client client;
	@Mock
	Technician technicien;	
	@Mock
	RequestDAO dao;

	@Before
	public void setUp() throws Exception {
		request = new Request("Test", "test", client, Enums.Category.OTHER);
	}

	@Test
	public void testSetAssignee() throws Exception {
		Mockito.doNothing().when(technicien).assignRequest(request);
		
		request.setAssignee(technicien);
		assertTrue(request.getAssignee().equals(technicien));
	}
	
	@Test (expected = IllegalStateException.class)
	public void testSetAssigneeException(){
		request.setAssignee(technicien);
		request.setAssignee(technicien);
	}
}
