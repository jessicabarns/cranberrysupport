package bean;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import utils.Enums;

@RunWith(MockitoJUnitRunner.class)
public class UserTest {
	Client client;
	
	@Mock
	Request request;

	@Before
	public void setUp() throws Exception {
		client = new Client("jessicabarns", "1234");
	}

	@Test
	public void testLogin() {
		assertTrue(client.login("JESSicaBARNs", "1234"));
	}
	
	@Test
	public void testWrongLogin(){
		assertFalse(client.login("jessicabarns", "5678"));
		assertFalse(client.login("jessicabarns1", "1234"));
	}
	
	@Test
	public void testGetPhone(){
		assertTrue(client.getPhone().equals(""));
		
		String phone = "5140005555";
		client.setPhone(phone);
		assertTrue(client.getPhone().equals(phone));
	}
	
	@Test
	public void testGetOffice(){
		assertTrue(client.getOffice().equals(""));
		
		String office = "4506660000";
		client.setOffice(office);
		assertTrue(client.getOffice().equals(office));
	}
	
	@Test
	public void testGetEmail(){
		assertTrue(client.getEmail().equals(""));
		
		String email = "jessica.barns@hotmail.com";
		client.setEmail(email);
		assertTrue(client.getEmail().equals(email));
	}
	
	@Test
	public void testAddRequest() throws Exception{
		addRequests(1);
		assertTrue(client.getRequests().contains(request));
	}
	
	@Test
	public void testGetLastName(){
		assertTrue(client.getLastname().equals(""));
		
		client.setLastname("Barns");
		assertTrue(client.getLastname().equals("Barns"));
	}
	
	@Test
	public void testGetFirstName(){
		assertTrue(client.getFirstname().equals(""));
		
		client.setFirstname("Jessica");
		assertTrue(client.getFirstname().equals("Jessica"));
	}
	
	public void addRequests(int numberOfRequests) throws Exception{
		for(int i = 0; i < numberOfRequests; i++){
			client.addRequest(request);
		}
	}
}
