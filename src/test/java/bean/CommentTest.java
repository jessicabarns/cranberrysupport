package bean;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CommentTest {
	Comment comment;
	@Mock
	Client author;

	@Before
	public void setUp() throws Exception {
		comment = new Comment("Hello", author);
		Mockito.when(author.getUsername()).thenReturn("jessicabarns");
	}

	@Test
	public void testToString() {
		assertTrue(comment.toString().equals("jessicabarns: Hello\n"));
	}
}
