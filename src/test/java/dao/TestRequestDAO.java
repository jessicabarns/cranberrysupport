package dao;
import static org.junit.Assert.*;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bean.Client;
import bean.Request;
import bean.Technician;
import exceptions.DatabaseException;
import utils.Enums.Category;
import utils.Enums.Status;

import org.apache.commons.io.FileUtils;

public class TestRequestDAO {
	RequestDAO requestDAO;
	static Client client01;
	static Technician tech01;
	Request request01;
	Request request02;
	
	@Before
	public void init(){
		try {
			request01 = new Request(1, "subject1", "description1", client01, null, Status.OPEN, Category.DESKTOP);
			request02 = new Request(2, "subject2", "description2", client01, tech01, Status.ONGOING, Category.DESKTOP);
			requestDAO = RequestDAO.getInstance();
			requestDAO.clearTable();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	@BeforeClass
	public static void initClass(){
		client01 = new Client("John", "Smith", "johnsmith", "password", "123-435-4352", "email", "office");
		tech01 = new Technician("Jade", "Mackenzie", "jademackenzie", "password", "123-435-4352", "email1", "office");
		try {
			UserDAO.getInstance().addUser(tech01);
			UserDAO.getInstance().addUser(client01);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown() throws DatabaseException{
		//requestDAO.clearTable();
	}

	@Test
	public void testAddRequest() throws FileNotFoundException, IOException, DatabaseException {
		requestDAO.addRequest(request01);
		
		Request expected = request01;
		Request actual = (Request) requestDAO.fetchRequestByStatus(Status.OPEN).get(0);
		assertEquals(expected.toString(), actual.toString());
	}
	
	@Test
	public void testFetchRequestByStatusEmpty() throws FileNotFoundException, IOException, DatabaseException {
		//non-existing request
		int expected = 0;
		int actual = requestDAO.fetchRequestByStatus(Status.OPEN).size();
		assertEquals(expected, actual);
	}
	@Test
	public void testFetchRequestByStatus() throws FileNotFoundException, IOException, DatabaseException {
		//existing request
		requestDAO.addRequest(request01);
		
		Request expected = request01;
		Request actual = (Request) requestDAO.fetchRequestByStatus(Status.OPEN).get(0);
		assertEquals(expected.toString(), actual.toString());
	}
	@Test
	public void testAssignRequestToTechnician() throws Exception {
		requestDAO.addRequest(request01);	

		requestDAO.assignRequestToTechnician((Technician)tech01, request01);
		
		//check that good amount of requests changed status
		String expected = tech01.getUsername();
		String actual = requestDAO.fetchRequestById(request01.getId()).getAssignee().getUsername();
		assertEquals(expected, actual);		
	}
	
	@Test
	public void testUploadFile() throws FileNotFoundException, IOException, DatabaseException{
		requestDAO.addRequest(request01);
		File expected = new File("properties.properties");
		request01.setFile(expected);
		
		requestDAO.uploadFile(request01);
		File request = requestDAO.fetchRequestById(request01.getId()).getFile();
		File actual = request01.getFile();
		assertTrue(FileUtils.contentEquals(request, actual));
	}
	@Test
	public void testFetchLastRequest() throws FileNotFoundException, IOException, DatabaseException {
		requestDAO.addRequest(request01);	
		requestDAO.addRequest(request02);

		int expected = request02.getId();
		int actual = requestDAO.fetchLastRequest().getId();
		assertEquals(expected, actual);
		
		//test if the request is associated with a file
		File file = new File("pom.xml");
		Request request03 = request02;
		request03.setFile(file);
		request03.setId(3);
		requestDAO.addRequest(request03);
		
		Request fetched = requestDAO.fetchLastRequest();
		assertTrue(FileUtils.contentEquals(file, fetched.getFile()));
	}
	
	@Test
	public void testIncrementId() throws FileNotFoundException, IOException, DatabaseException {
		request01 = new Request(requestDAO.nextId(), "subject", "description", client01, tech01, Status.OPEN, Category.DESKTOP);
		requestDAO.addRequest(request01);
		request02 = new Request(requestDAO.nextId(), "subject", "description", client01, tech01, Status.OPEN, Category.DESKTOP);
		int idFirstRequest = request01.getId();
		int idSecondRequest = request02.getId();
		
		int expected = 1;
		int actual = idSecondRequest - idFirstRequest;
		assertEquals(expected, actual);
	}
	
	@Test
	public void testAddCommentToRequest() throws DatabaseException, Exception{
		requestDAO.addRequest(request01);	
		request01.addComment("commentaire pertinent", client01);
		
		int expected = 1;
		int actual = requestDAO.fetchCommentsByRequestId(request01.getId()).size();
		assertEquals(expected, actual);
		
		//more than one comment
		request01.addComment("commentaire pertinent", client01);
		expected = 2;
		actual = requestDAO.fetchCommentsByRequestId(request01.getId()).size();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdateRequest() throws Exception{
		requestDAO.addRequest(request01);
		request01.setCategory(Category.WEB_SERVICE);
		request01.setStatus(Status.OPEN);
		
		requestDAO.updateRequest(request01);
		
		Request expected = request01;
		Request actual = requestDAO.fetchLastRequest();
		assertEquals(expected.toString(), actual.toString());
	}
}
