package dao;
import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bean.Administrator;
import bean.Client;
import bean.Request;
import bean.Technician;
import bean.User;
import exceptions.DatabaseException;
import utils.Enums.Category;
import utils.Enums.Status;

public class TestUtilisateursDAO {
	
	UserDAO userDAO;
	Client client01;
	Technician tech01;
	Technician tech02;
	Administrator admin;
	@Before
	public void init() throws DatabaseException{
		client01 = new Client("John", "Smith", "johnsmith", "password", "123-435-4352", "email", "office");
		tech01 = new Technician("Jade", "Mackenzie", "jademackenzie", "password", "123-435-4352", "email1", "office");
		tech02 = new Technician("Emily", "Cambell", "emilycambell", "password", "123-435-4352", "email2", "office");
		admin = new Administrator("George", "Washington", "joji", "password", "123-435-4352", "email3", "office4");

		try {
			userDAO = UserDAO.getInstance();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		userDAO.clearTable();
	}
	@After
	public void tearDown() throws DatabaseException{
		userDAO.clearTable();
	}
	
	@Test
	public void testAddUser() throws Exception {
		userDAO.addUser(admin);
		userDAO.addUser(client01);
		User expected = client01;
		User actual = userDAO.fetchUserByUsername(client01.getUsername());
		
		assertTrue(expected.sameAs(actual));
	}
	
	@Test
	public void testFetchUserAndRequestsByUsername() throws Exception{
		Request request01 = new Request(1, "subject1", "description1", client01, null, Status.OPEN, Category.DESKTOP);
		userDAO.addUser(client01);
		client01.addRequest(request01);
		
		User expected = client01;
		User actual = userDAO.fetchUserAndRequestsByUsername(client01.getUsername());
		assertTrue(expected.sameAs(actual));
	}
	
	@Test
	public void testFetchUser() throws Exception {
		//user not in DB
		assertEquals(null, userDAO.fetchUserByUsername(client01.getUsername()));
		
		//user in DB, simultaneously tests for addUser
		//test with client
		userDAO.addUser(client01);
		User expected = client01;
		User actual = userDAO.fetchUserByUsername(client01.getUsername());
		assertTrue(expected.sameAs(actual));
		
		//test with tech
		userDAO.addUser(tech01);
		expected = tech01;
		actual = userDAO.fetchUserByUsername(tech01.getUsername());
		assertTrue(expected.sameAs(actual));
		
	}
	
	@Test
	public void testFetchUserByUserName() throws Exception {
		// existing user
		userDAO.addUser(client01);
		User expected = client01;
		User actual = userDAO.fetchUserByUsername("johnsmith");
		assertTrue(expected.sameAs(actual));
		
		// non existing user
		expected = null;
		actual = userDAO.fetchUserByUsername("bobafett");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetListTechnician() throws DatabaseException{
		userDAO.addUser(client01);
		
		//no technicians added
		int expectedAmount = 0;
		int actualAmount = userDAO.fetchListTechnician().size();
		assertEquals(expectedAmount, actualAmount);
		
		//one technician
		userDAO.addUser(tech01);
		expectedAmount = 1;
		actualAmount = userDAO.fetchListTechnician().size();
		assertEquals(expectedAmount, actualAmount);
		
		//two technician
		userDAO.addUser(tech02);
		expectedAmount = 2;
		actualAmount = userDAO.fetchListTechnician().size();
		assertEquals(expectedAmount, actualAmount);
	}
	
	@Test
	public void testDeleteUser() throws Exception{
		userDAO.addUser(client01);
		
		userDAO.deleteUser(client01);
		User actual = userDAO.fetchUserByUsername(client01.getUsername());
		assertNull(actual);
	}
	
	@Test
	public void testUpdateUser() throws Exception{
		userDAO.addUser(client01);
		
		client01.setFirstname("afterfirstname");
		client01.setLastname("afterlastname");
		client01.setEmail("afteremail");
		client01.setOffice("afteroffice");
		client01.setPhone("afterphone");
		
		userDAO.updateUser(client01);
		
		User requested = userDAO.fetchUserByUsername(client01.getUsername());
		assertTrue(client01.sameAs(requested));
	}
	
	
}
