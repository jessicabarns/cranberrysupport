import java.io.FileNotFoundException;

import bean.Administrator;
import bean.Client;
import bean.Technician;
import dao.UserDAO;
import exceptions.DatabaseException;

public class DatabaseFiller {

	//USED ONLY FOR FILLING THE DATABASE WITH USERS FOR TESTING/DEBUGGING
	public static void main(String[] args) throws DatabaseException {
		Client client01 = new Client("John", "Smith", "johnsmith", "password", "123-435-4352", "email", "office");
		Technician tech01 = new Technician("Jade", "Mackenzie", "bob", "password", "123-435-4352", "email1", "office");
		Technician tech02 = new Technician("Emily", "Cambell", "emilycambell", "password", "123-435-4352", "email2", "office");
		Administrator admin = new Administrator("George", "Washington", "joji", "password", "123-435-4352", "email3", "office4");
		
		try {
			//UserDAO.getInstance().addUser(client01);
			UserDAO.getInstance().addUser(tech01);
			UserDAO.getInstance().addUser(tech02);
			UserDAO.getInstance().addUser(admin);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
