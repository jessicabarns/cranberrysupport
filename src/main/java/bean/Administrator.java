package bean;

import java.io.FileNotFoundException;

import dao.UserDAO;
import exceptions.DatabaseException;

public class Administrator extends Technician {

	/**
	 * empty constructor for DAO
	 */
	public Administrator() {
		super();
	}

	public Administrator(String username, String password) {
		super(username, password);
	}

	public Administrator(String firstname, String lastname, String username, String password) {
		super(firstname, lastname, username, password);
	}

	/**
	 * full constructor for tests
	 * 
	 * @param firstname
	 * @param lastname
	 * @param username
	 * @param password
	 * @param phone
	 * @param email
	 * @param office
	 */
	public Administrator(String firstname, String lastname, String username, String password, String phone,
			String email, String office) {
		super(firstname, lastname, username, password, phone, email, office);
	}

	/**
	 * add an user to the database
	 * 
	 * @param user
	 * @throws FileNotFoundException
	 * @throws DatabaseException
	 */
	public void addUser(User user) throws FileNotFoundException, DatabaseException {
		UserDAO.getInstance().addUser(user);
	}

	/**
	 * delete an user from the database
	 * 
	 * @param user
	 * @throws FileNotFoundException
	 * @throws DatabaseException
	 */
	public void deleteUser(User user) throws FileNotFoundException, DatabaseException {
		UserDAO.getInstance().deleteUser(user);
	}

	/**
	 * verify if the user is the same as this
	 * 
	 * @param user
	 * @return true if same, false otherwise
	 */
	@Override
	public boolean sameAs(User user) {
		boolean flag = false;
		if (user instanceof Administrator && getUsername().equals(user.getUsername())
				&& getFirstname().equals(user.getFirstname()) && getLastname().equals(user.getLastname())
				&& getPassword().equals(user.getPassword()) && getEmail().equals(user.getEmail())
				&& getPhone().equals(user.getPhone()) && getOffice().equals(user.getOffice())) {
			flag = true;
		}
		return flag;
	}

}
