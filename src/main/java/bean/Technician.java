package bean;

import java.util.ArrayList;

import dao.RequestDAO;
import utils.Enums;

public class Technician extends User {

	/**
	 * empty constructor for DAO
	 */
	public Technician() {
		super();
	}

	public Technician(String username, String password) {
		super(username, password);
	}

	public Technician(String firstname, String lastname, String username, String password) {
		super(firstname, lastname, username, password);
	}

	/**
	 * full constructor for tests
	 * 
	 * @param firstname
	 * @param lastname
	 * @param username
	 * @param password
	 * @param phone
	 * @param email
	 * @param office
	 */
	public Technician(String firstname, String lastname, String username, String password, String phone, String email,
			String office) {
		super(firstname, lastname, username, password, phone, email, office);
	}

	/**
	 * assign a request to the technician
	 * 
	 * @param request
	 */
	public void assignRequest(Request request) {
		addRequest(request);
	}

	/**
	 * get the report of the requests assigned to the technician
	 * 
	 * @return the report
	 */
	public String getRequestsReport() {
		String result = new String();
		for (Enums.Status status : Enums.Status.values()) {
			result += "Status " + status.getValue() + ": ";
			try {
				ArrayList<Request> requests = RequestDAO.getInstance().fetchRequestByStatus(status);
				int counter = 0;
				for (Request request : requests) {
					if (sameAs(request.getAssignee())) {
						counter++;
					}
				}
				result += counter;
			} catch (Exception e) {
				e.printStackTrace();
			}
			result += "\n";
		}
		return result;
	}

	/**
	 * verify if the user is the same as this
	 * 
	 * @param user
	 * @return true if same, false otherwise
	 */
	@Override
	public boolean sameAs(User user) {
		boolean flag = false;
		if (user instanceof Technician && getUsername().equals(user.getUsername())
				&& getFirstname().equals(user.getFirstname()) && getLastname().equals(user.getLastname())
				&& getPassword().equals(user.getPassword()) && getEmail().equals(user.getEmail())
				&& getPhone().equals(user.getPhone()) && getOffice().equals(user.getOffice())) {
			flag = true;
		}
		return flag;
	}

	/**
	 * obtain the requests assigned to the technician
	 * 
	 * @return the requests assigned to the technician
	 */
	@Override
	public ArrayList<Request> getRequests() {
		ArrayList<Request> assignedRequests = new ArrayList<>();
		for (Request request : requests) {
			if (sameAs(request.getAssignee())) {
				assignedRequests.add(request);
			}
		}
		return assignedRequests;
	}
}