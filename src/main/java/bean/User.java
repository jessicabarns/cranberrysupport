package bean;

import java.util.ArrayList;

import utils.Enums;

public abstract class User {

	protected String lastname;
	protected String firstname;
	protected String username;
	protected String password;
	protected String phone;
	protected String email;
	protected String office;
	protected ArrayList<Request> requests;

	/**
	 * empty constructor for DAO
	 */
	public User() {
		requests = new ArrayList<Request>();
	}

	public User(String username, String password) {
		this.username = username;
		this.password = password;
		requests = new ArrayList<Request>();
	}

	public User(String firstname, String lastname, String username, String password) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		requests = new ArrayList<Request>();
	}

	/**
	 * constructor for tests (includess all variables)
	 * 
	 * @param firstname
	 * @param lastname
	 * @param username
	 * @param password
	 * @param phone
	 * @param email
	 * @param office
	 */
	public User(String firstname, String lastname, String username, String password, String phone, String email,
			String office) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		this.phone = phone;
		this.email = email;
		this.office = office;
		requests = new ArrayList<Request>();
	}

	/**
	 * verify if the login is correct
	 * 
	 * @param username
	 * @param password
	 * @return true if correct, false otherwise
	 */
	public boolean login(String username, String password) {
		if (username.equalsIgnoreCase(this.username) && password.equals(this.password)) {
			return true;
		}
		return false;
	}

	public String getUsername() {
		return username;
	}

	public String getLastname() {
		if (lastname == null) {
			return "";
		}
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		if (firstname == null) {
			return "";
		}
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		if (phone == null) {
			return "";
		}
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		if (email == null) {
			return "";
		}
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOffice() {
		if (office == null) {
			return "";
		}
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setRequests(ArrayList<Request> requests) {
		this.requests = requests;
	}

	/**
	 * get the user requests with a certain status
	 * 
	 * @param status
	 * @return the list of requests with the specified status
	 */
	public ArrayList<Request> getRequestsByStatus(Enums.Status status) {
		ArrayList<Request> requestsWithStatus = new ArrayList<>();
		for (Request request : getRequests()) {
			if (request.getStatus().equals(status)) {
				requestsWithStatus.add(request);
			}
		}
		return requestsWithStatus;
	}

	/**
	 * add a request to the user
	 * 
	 * @param request
	 */
	public void addRequest(Request request) {
		if (!getRequests().contains(request)) {
			requests.add(request);
		}
	}

	/**
	 * obtain the requests of the user
	 * 
	 * @return the requests of the user
	 */
	public abstract ArrayList<Request> getRequests();

	/**
	 * verify if the user is the same as this
	 * 
	 * @param user
	 * @return true if same, false otherwise
	 */
	public abstract boolean sameAs(User user);

}