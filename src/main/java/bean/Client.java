package bean;

import java.util.ArrayList;

public class Client extends User {

	/**
	 * empty constructor for DAO
	 */
	public Client() {
		super();
	}

	public Client(String username, String password) {
		super(username, password);
	}

	public Client(String firstname, String lastname, String username, String password) {
		super(firstname, lastname, username, password);
	}

	/**
	 * full constructor for tests
	 * 
	 * @param firstname
	 * @param lastname
	 * @param username
	 * @param password
	 * @param phone
	 * @param email
	 * @param office
	 */
	public Client(String firstname, String lastname, String username, String password, String phone, String email,
			String office) {
		super(firstname, lastname, username, password, phone, email, office);
	}

	/**
	 * verify if the user is the same as this
	 * 
	 * @param user
	 * @return true if same, false otherwise
	 */
	public boolean sameAs(User user) {
		boolean flag = false;
		if (user instanceof Client && getUsername().equals(user.getUsername())
				&& getFirstname().equals(user.getFirstname()) && getLastname().equals(user.getLastname())
				&& getPassword().equals(user.getPassword()) && getEmail().equals(user.getEmail())
				&& getPhone().equals(user.getPhone()) && getOffice().equals(user.getOffice())) {
			flag = true;
		}
		return flag;
	}

	/**
	 * get the requests of this Client
	 */
	@Override
	public ArrayList<Request> getRequests() {
		return requests;
	}

	@Override
	public String toString() {
		return "Client [lastname=" + lastname + ", firstname=" + firstname + ", username=" + username + ", password="
				+ password + ", phone=" + phone + ", email=" + email + ", office=" + office + ", requests=" + requests
				+ "]";
	}

}