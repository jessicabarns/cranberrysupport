package bean;

public class Comment {

	private String comment;
	private User author;

	private int id;
	private static int idCount;

	/**
	 * empty constructor for DAO
	 */
	public Comment() {

	}

	public Comment(String comment, User author) {
		this.comment = comment;
		this.author = author;
		this.id = ++idCount;
	}

	public User getAuthor() {
		return author;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * return the comment to this format -> username: comment
	 */
	public String toString() {
		return author.getUsername() + ": " + comment + "\n";
	}
}