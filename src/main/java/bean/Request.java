package bean;

import java.io.File;
import java.util.ArrayList;

import dao.RequestDAO;
import exceptions.DatabaseException;
import utils.Enums.Category;
import utils.Enums.Status;

public class Request {
	private Integer id;
	private String subject;
	private String description;
	private File file;
	private boolean approved;

	private User requester;
	private Technician assignee;
	private Status status;
	private Category category;

	private ArrayList<Comment> comments;

	/**
	 * empty constructor for DAO
	 */
	public Request() {
		this.approved = false;
		comments = new ArrayList<Comment>();
	}

	public Request(String subject, String description, Client requester, Category category) throws Exception {
		this.approved = false;
		this.subject = subject;
		this.description = description;
		this.requester = requester;
		this.category = category;

		id = RequestDAO.getInstance().nextId();
		status = Status.OPEN;
		comments = new ArrayList<Comment>();
		requester.addRequest(this);
	}

	public Request(String subject, String description, User requester, Category category) throws Exception {
		this.approved = false;
		this.requester = requester;
		this.subject = subject;
		this.description = description;
		this.requester = requester;
		this.category = category;

		id = RequestDAO.getInstance().nextId();
		status = Status.OPEN;
		comments = new ArrayList<Comment>();
		requester.addRequest(this);
	}

	/**
	 * full constructor for tests
	 * 
	 * @param id
	 * @param subject
	 * @param description
	 * @param requester
	 * @param assignee
	 * @param status
	 * @param category
	 */
	public Request(Integer id, String subject, String description, User requester, Technician assignee, Status status,
			Category category) {
		this.approved = false;
		this.id = id;
		this.subject = subject;
		this.description = description;
		this.requester = requester;
		this.assignee = assignee;
		this.status = status;
		this.category = category;
		comments = new ArrayList<Comment>();

	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFilePath() {
		return file.getAbsolutePath();
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getRequester() {
		return requester;
	}

	public void setRequester(User requester) {
		this.requester = requester;
	}

	public Status getStatus() {
		return status;
	}

	/**
	 * change the status and update it in the database if it's different then
	 * what it was before
	 * 
	 * @param status
	 * @throws DatabaseException
	 * @throws Exception
	 */
	public void setStatus(Status status) throws DatabaseException, Exception {
		if (hasStatusChanged(status)) {
			this.status = status;
			RequestDAO.getInstance().updateRequest(this);
		} else {
			this.status = status;
		}
	}

	/**
	 * know if the status is different then what it was before
	 * 
	 * @param status
	 * @return true if it's different, false otherwise
	 */
	private boolean hasStatusChanged(Status status) {
		return this.status != null && status != this.status;
	}

	public Category getCategory() {
		return category;
	}

	/**
	 * change the category and update it if it's different then what it was
	 * before
	 * 
	 * @param category
	 * @throws DatabaseException
	 * @throws Exception
	 */
	public void setCategory(Category category) throws DatabaseException, Exception {
		if (hasCategoryChanged(category)) {
			this.category = category;
			RequestDAO.getInstance().updateRequest(this);
		} else {
			this.category = category;
		}
	}

	/**
	 * know if the category is different then what it was before
	 * 
	 * @param category
	 * @return true if it's different, false otherwise
	 */
	private boolean hasCategoryChanged(Category category) {
		return this.category != null && category != this.category;
	}

	public ArrayList<Comment> getComments() {
		return comments;
	}

	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}

	public Technician getAssignee() {
		return assignee;
	}

	/**
	 * set an assignee to the Request and set the request to the Technician. The
	 * assignee can't change.
	 * 
	 * @param assignee
	 */
	public void setAssignee(Technician assignee) {
		if (this.assignee != null) {
			throw new IllegalStateException("there's already an assignee to this request");
		}

		this.assignee = (Technician) assignee;
		this.assignee.assignRequest(this);
	}

	/**
	 * upload the file to the database
	 * 
	 * @param file
	 */
	public void uploadFile(File file) {
		this.setFile(file);
		try {
			RequestDAO.getInstance().uploadFile(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * add a comment and update it in the database
	 * 
	 * @param comment
	 * @param user
	 * @throws DatabaseException
	 * @throws Exception
	 */
	public void addComment(String comment, User user) throws DatabaseException, Exception {
		Comment nextComment = new Comment(comment, user);
		comments.add(nextComment);
		RequestDAO.getInstance().addCommentToRequest(this, nextComment);
	}

	@Override
	public String toString() {
		return "Request [id=" + id + ", subject=" + subject + ", description=" + description + ", file=" + file
				+ ", requester=" + requester.getUsername() + ", status=" + status + ", category=" + category
				+ ", approved= " + approved + "]";
	}
}