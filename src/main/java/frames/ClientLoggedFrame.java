package frames;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;

import bean.Comment;
import bean.Request;
import bean.User;
import dao.RequestDAO;
import exceptions.DatabaseException;

public class ClientLoggedFrame extends javax.swing.JFrame {

    User user;
    String clientname;
    ArrayList<Request> requestList;
    Request toPrint;
    JFrame prec;
    private File file;
    private boolean showComments = false;

    /** Creates new form ClientLoggedFrame */
    public ClientLoggedFrame(User client) {
        initComponents();
        this.setVisible(true);
        user = client;
        requestList = client.getRequests();

        
        //Le code qui suit initialise la liste de requête du client
        DefaultListModel a = new DefaultListModel();

        if (client.getRequests().isEmpty()) {
            a.addElement("Vous n'avez pas de requ�te encore.");
        } else {
            for (int i = 0; i < client.getRequests().size(); i++) {
                a.addElement(client.getRequests().get(i).getSubject());

            }

        }
        listRequeteList.setModel(a); //peint la liste
        commentArea.setVisible(false);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        listRequeteList = new javax.swing.JList();
        requetesLbl = new javax.swing.JLabel();
        addRequeteBtn = new javax.swing.JButton();
        quitBtn = new javax.swing.JButton();
        modifBtn = new javax.swing.JButton();
        fichierBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        requeteArea = new javax.swing.JTextArea();
        voirComsBtn = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        commentArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        listRequeteList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "bouba" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listRequeteList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listRequeteListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(listRequeteList);

        requetesLbl.setText("Vos requ�tes:");

        addRequeteBtn.setText("Faire une nouvelle requ�te");
        addRequeteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addRequeteBtnActionPerformed(evt);
            }
        });

        quitBtn.setText("Quitter");
        quitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitBtnActionPerformed(evt);
            }
        });

        modifBtn.setText("Ajouter un commentaire");
        modifBtn.setEnabled(false);
        modifBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifBtnActionPerformed(evt);
            }
        });

        fichierBtn.setText("Ajouter un fichier");
        fichierBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fichierBtnActionPerformed(evt);
            }
        });

        jLabel1.setText("Modifier la requ�te s�lectionn�e:");

        requeteArea.setColumns(20);
        requeteArea.setEditable(false);
        requeteArea.setLineWrap(true);
        requeteArea.setRows(5);
        jScrollPane2.setViewportView(requeteArea);

        voirComsBtn.setBackground(new java.awt.Color(102, 153, 255));
        voirComsBtn.setText("Voir les commentaires");
        voirComsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voirComsBtnActionPerformed(evt);
            }
        });

        commentArea.setColumns(20);
        commentArea.setLineWrap(true);
        commentArea.setRows(5);
        commentArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                try {
					commentAreaKeyPressed(evt);
				} catch (Exception e) {
					e.printStackTrace();
				}
            }
        });
        jScrollPane3.setViewportView(commentArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addRequeteBtn)
                    .addComponent(requetesLbl)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(quitBtn))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(voirComsBtn)
                            .addComponent(fichierBtn)
                            .addComponent(modifBtn)
                            .addComponent(jLabel1)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(addRequeteBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(requetesLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(quitBtn))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(voirComsBtn)
                                .addGap(26, 26, 26)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fichierBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(modifBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)))))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Bouton Ajouter une requête
    private void addRequeteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addRequeteBtnActionPerformed
        this.setVisible(false);
        newRequeteFrame nouvelleRequete = new newRequeteFrame(user, this);
    }//GEN-LAST:event_addRequeteBtnActionPerformed

    //Lorsqu'on sélectionne un élément de la liste
    //la région à coté se met a jour avec les informations de la requete sélectionnée
    private void listRequeteListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listRequeteListValueChanged
        updateRequete();
    }//GEN-LAST:event_listRequeteListValueChanged

    //Bouton quitter
    private void quitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitBtnActionPerformed
        	System.exit(0);  
    }//GEN-LAST:event_quitBtnActionPerformed

    //l'ajout d'un fichier passe pas la sélection à l'aide d'un JFileChooser
    private void fichierBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fichierBtnActionPerformed
        fileChooser.showOpenDialog(prec);
        file = fileChooser.getSelectedFile();
        try {
            requestList.get(listRequeteList.getSelectedIndex()).setFile(file);
        } catch (IndexOutOfBoundsException e) {
        }

        File f = fileChooser.getSelectedFile();
        if (f != null) {
            try {
                FileWriter newFile = new FileWriter("dat/" + f.getName());
            } catch (IOException ex) {
                Logger.getLogger(newRequeteFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (file.exists()) {
                fichierAddOkFrame ok = new fichierAddOkFrame();
                ok.setVisible(true);
            }
        }
    }//GEN-LAST:event_fichierBtnActionPerformed

    private void modifBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifBtnActionPerformed
        commentArea.setVisible(true);
        commentArea.requestFocus();
    }//GEN-LAST:event_modifBtnActionPerformed

    //Affiche les commentaires
    private void voirComsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_voirComsBtnActionPerformed
        if (toPrint != null) {
            if (!showComments) {
                String s = "";
                for (Comment c : toPrint.getComments()) {
                    s += c.toString();
                }
                requeteArea.setText(s);
                showComments = true;
                voirComsBtn.setText("Voir la rêquete");
                fichierBtn.setEnabled(false);
                modifBtn.setEnabled(true);
            } else {
                updateRequete();
            }
        }
    }//GEN-LAST:event_voirComsBtnActionPerformed
    //Mise a jour de la région d'affichage de la requête

    private void updateRequete() {
        toPrint = requestList.get(listRequeteList.getSelectedIndex());
        requeteArea.setText("Sujet: " + toPrint.getSubject()
                + "\nDescription: " + toPrint.getDescription()
                + "\nCatégorie: " + toPrint.getCategory().toString()
                + "\nStatut: " + toPrint.getStatus().toString());
        showComments = false;
        voirComsBtn.setText("Voir les commentaires");
        fichierBtn.setEnabled(true);
        modifBtn.setEnabled(false);
    }

    //Ajoute un commentaire qui a été entré dans la ligne
    //réagit à ENTER
    private void commentAreaKeyPressed(java.awt.event.KeyEvent evt) throws DatabaseException, Exception {//GEN-FIRST:event_commentAreaKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            toPrint.addComment(commentArea.getText(), user);
            String s = "";
            for (Comment c : toPrint.getComments()) {
                s += c.toString();
            }
            requeteArea.setText(s);
            commentArea.setVisible(false);
            modifBtn.requestFocus();
        }
    }//GEN-LAST:event_commentAreaKeyPressed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addRequeteBtn;
    private javax.swing.JTextArea commentArea;
    private javax.swing.JButton fichierBtn;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JList listRequeteList;
    private javax.swing.JButton modifBtn;
    private javax.swing.JButton quitBtn;
    private javax.swing.JTextArea requeteArea;
    private javax.swing.JLabel requetesLbl;
    private javax.swing.JButton voirComsBtn;
    // End of variables declaration//GEN-END:variables
}
