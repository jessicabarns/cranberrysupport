package exceptions;

public class DatabaseException extends Exception {
	private static final long serialVersionUID = -1845773185335215452L;
	
	public DatabaseException(String message){
		super(message);
	}
	
}
