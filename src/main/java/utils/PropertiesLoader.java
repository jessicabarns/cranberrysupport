package utils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
	public static Properties loadProperties(){
		Properties prop = new Properties();
		InputStream in;
		try {
			in = new FileInputStream("properties.properties");
			prop.load(in);
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			return prop;
		}
	}
}
