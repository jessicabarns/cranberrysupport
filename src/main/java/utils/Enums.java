package utils;

public class Enums {

	public enum Status {
		OPEN("Opened"),
		ONGOING("Ongoing"),
		DROPPED("Dropped"),
		SUCCESS("Success");

		private String value;

		private Status(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public boolean isFinal() {
			return this.equals(DROPPED) || this.equals(SUCCESS);
		}

		// Retourne le statut en fonction d'une string donnée
		public static Status getStatusFromString(String str) throws Exception {
			for (Status status : Status.values()) {
				if (status.getValue().equals(str)) {
					return status;
				}
			}
			throw new Exception(str + " is not associated with a status");
		}
	}

	public enum Category {

		DESKTOP("Poste de travail"),
		SERVER("Serveur"),
		WEB_SERVICE("Service web"),
		USER_ACCOUNT("Compte usager"),
		OTHER("Autre");
		
		String value;

		private Category(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		// Retourne la Cat�gorie en fonction d'une String donnée
		public static Category getCategoryFromString(String statusStr) throws Exception {
			for (Category categorie : Category.values()) {
				if (categorie.getValue().equals(statusStr)) {
					return categorie;
				}
			}
			throw new Exception(statusStr + " is not associated with a category");
		}
	}
}
