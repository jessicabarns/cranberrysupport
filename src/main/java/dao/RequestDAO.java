package dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import bean.Comment;
import bean.Request;
import bean.Technician;
import bean.User;
import exceptions.DatabaseException;
import utils.Enums;
import utils.PropertiesLoader;
import utils.Enums.Category;
import utils.Enums.Status;

public class RequestDAO {

	private String url;
	private String username;
	private String password;
	private Connection connection;
	private static RequestDAO instance = null;

	/**
	 * private constructor
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws DatabaseException
	 */
	private RequestDAO() throws FileNotFoundException, IOException, DatabaseException {
		try {
			loadDBProperties();
			connectToDB();
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}

	}

	/**
	 * used to start a connection to the database
	 * 
	 * @throws DatabaseException
	 */
	private void connectToDB() throws DatabaseException {
		try {
			connection = DriverManager.getConnection(url, username, password);
		} catch (Exception e) {

			throw new DatabaseException(e.getMessage());
		}
	}

	/**
	 * class that load the login credentials and address of the database for the
	 * login
	 * 
	 * @throws IOException
	 */
	private void loadDBProperties() throws IOException {
		Properties prop = PropertiesLoader.loadProperties();

		url = prop.getProperty("dbAdress");
		username = prop.getProperty("dbusername");
		password = prop.getProperty("dbpassword");
	}

	/**
	 * Manages the singleton
	 * 
	 * @return instance of RequestDAO
	 * @throws Exception
	 */
	public static RequestDAO getInstance() throws Exception {
		if (instance == null) {
			instance = new RequestDAO();
		}
		return instance;
	}

	/**
	 * used to add a request to the database, can either take a file or not
	 * 
	 * @param request
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws DatabaseException
	 */
	public void addRequest(Request request) throws FileNotFoundException, IOException, DatabaseException {
		try {
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO request 	(idrequest, status, category, subject, description, requesterid, assigneeid)"
							+ " VALUES(?,?,?,?,?,?,?)");

			statement.setInt(1, request.getId());
			statement.setString(2, request.getStatus().getValue()); 
			statement.setString(3, request.getCategory().getValue());
			statement.setString(4, request.getSubject());
			statement.setString(5, request.getDescription());
			statement.setString(6, request.getRequester().getUsername());
			
			String assigneeId = null;
			if(request.getAssignee() != null){
				assigneeId = request.getAssignee().getUsername();
			}
			statement.setString(7, assigneeId);

			statement.executeUpdate();
			statement.close();

			if (request.getFile() != null) {
				uploadFile(request);
			}
			
			ArrayList<Comment> comments = request.getComments();
			for(Comment comment :  comments){
				addCommentToRequest(request, comment);
			}
			
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
	}


	/**
	 * used to upload a file to the database
	 *
	 * @param request
	 * @throws DatabaseException
	 */
	public void uploadFile(Request request) throws DatabaseException {
		try {
			PreparedStatement statement = connection
					.prepareStatement("INSERT INTO file (filename, file, requestid)" + "	VALUES(?,?,?)");

			statement.setString(1, request.getFile().getName());
			FileInputStream input = new FileInputStream(request.getFile());
			statement.setBinaryStream(2, input);
			statement.setInt(3, request.getId());

			statement.executeUpdate();
			input.close();
			statement.close();
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
	}
	
	/**
	 * fetches the file associated with a specific request id
	 * 
	 * @param requestId
	 * @return File
	 * @throws DatabaseException
	 */
	private File fetchFile(int requestId) throws DatabaseException {
		try {
			File file = null;
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM file WHERE requestid = ?");
			statement.setInt(1, requestId);
  
			statement.executeQuery();

			ResultSet rs = statement.getResultSet();
			while (rs.next()) {
				file = new File("dat/" +rs.getString(2));
				FileOutputStream output = new FileOutputStream(file);
				InputStream input = rs.getBinaryStream(3);
				byte[] buffer = new byte[1];
				if (input != null) {
					while (input.read(buffer) > 0) {
						output.write(buffer);
					}
				}
				output.close();
			}

			statement.close();
			return file;
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
	}

	/**
	 * fetches all the requests with a certain status
	 * 
	 * @param status
	 * @return Arraylist<Request>
	 * @throws DatabaseException
	 */
	public ArrayList<Request> fetchRequestByStatus(Enums.Status status) throws DatabaseException {
		ArrayList<Request> requests = new ArrayList<Request>();
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM request WHERE status = ?");
			statement.setString(1, status.getValue());

			statement.executeQuery();

			ResultSet rs = statement.getResultSet();

			requests = parseRequestListFromResultSet(rs, null);
			
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
		return requests;
	}
	
	
	/**
	 * used to fetch the requests that belong to the specified user (either as a requester or as an assignee
	 * @param user
	 * @return ArrayList<Request>
	 * @throws DatabaseException
	 */
	public ArrayList<Request> fetchRequestsByUser(User user) throws DatabaseException {
		ArrayList<Request> requests = new ArrayList<Request>();
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM request WHERE requesterid = ? OR assigneeid = ?");
			statement.setString(1, user.getUsername());
			statement.setString(2, user.getUsername());

			statement.executeQuery();

			ResultSet rs = statement.getResultSet();
			
			requests = parseRequestListFromResultSet(rs, user);
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
		return requests;
	}
	
	/**
	 * fetches a request by its id
	 * 
	 * @param id
	 * @return searched request
	 * @throws DatabaseException
	 */
	public Request fetchRequestById(int id) throws DatabaseException {
		Request request = null;
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM request WHERE idrequest = ?");
			statement.setInt(1, id);
			statement.executeQuery();

			ResultSet rs = statement.getResultSet();
			request = parseRequestListFromResultSet(rs, null).get(0);
			
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
		return request;
	}

	/**
	 * 
	 * parser used by all request queries to make a lis of request out of the resultset
	 * @param rs
	 * @param requester
	 * @return ArrayList<Request>
	 * @throws SQLException
	 * @throws Exception
	 * @throws FileNotFoundException
	 * @throws DatabaseException
	 */
	private ArrayList<Request> parseRequestListFromResultSet(ResultSet rs, User requester)
			throws SQLException, Exception, FileNotFoundException, DatabaseException {
		ArrayList<Request> requests = new ArrayList<Request>();
		while (rs.next()){
			Request currentRequest = new Request();
			currentRequest.setId(rs.getInt(1));
			currentRequest.setStatus(Status.getStatusFromString(rs.getString(2)));
			currentRequest.setCategory(Category.getCategoryFromString(rs.getString(3)));
			currentRequest.setSubject(rs.getString(4));
			currentRequest.setDescription(rs.getString(5));
		
			
			if(requester == null){
				currentRequest.setRequester(UserDAO.getInstance().fetchUserByUsername(rs.getString(6)));				
			}else{
				currentRequest.setRequester(requester);
			}
			
			Technician assignee = (Technician) UserDAO.getInstance().fetchUserByUsername(rs.getString(7));
			if(assignee != null){
				currentRequest.setAssignee(assignee);
			}
			
			currentRequest.setApproved(rs.getBoolean(8));
			
			File file = fetchFile(currentRequest.getId());
			if(file != null){
				currentRequest.setFile(file);
			}
			
			currentRequest.setComments(fetchCommentsByRequestId(currentRequest.getId()));
			requests.add(currentRequest);
		}
		return requests;
	}
	
	/**
	 * used to assign a request to a technician, automatically switching it to
	 * an ONGOING status
	 * 
	 * @param technicien
	 * @param selectionee
	 * @throws Exception
	 */
	public void assignRequestToTechnician(Technician technicien, Request selectionee) throws Exception {
		try {
			PreparedStatement statement = connection
					.prepareStatement("UPDATE request " + "SET assigneeid=?, status=? " + "WHERE idrequest=? ");

			statement.setString(1, technicien.getUsername());
			statement.setString(2, Status.ONGOING.getValue());
			statement.setInt(3, selectionee.getId());

			statement.executeUpdate();
			statement.close();
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
	}

	/**
	 * used to fetch the latest request that was inserted
	 * 
	 * @return Request
	 * @throws DatabaseException
	 */
	public Request fetchLastRequest() throws DatabaseException {
		Request request = null;
		try {
			PreparedStatement statement = connection
					.prepareStatement("SELECT * FROM request " + "ORDER BY idrequest DESC LIMIT 1");
			statement.executeQuery();

			ResultSet rs = statement.getResultSet();
			while (rs.next()) {
				request = new Request();
				request.setId(rs.getInt(1));
				request.setStatus(Status.getStatusFromString(rs.getString(2)));
				System.out.println(rs.getString(3));
				request.setCategory(Category.getCategoryFromString(rs.getString(3)));
				request.setSubject(rs.getString(4));
				request.setDescription(rs.getString(5));
				request.setRequester(UserDAO.getInstance().fetchUserByUsername(rs.getString(6)));
				Technician assignee = (Technician) UserDAO.getInstance().fetchUserByUsername(rs.getString(7));
				request.setApproved(rs.getBoolean(8));
				
				if(assignee != null){
					request.setAssignee(assignee);
				}
				File file = fetchFile(rs.getInt(1));
				if (file != null) {
					request.setFile(file);
				}
				request.setComments(fetchCommentsByRequestId(request.getId()));
			}
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
		return request;
	}

	/**
	 * used to find the next available id for a request
	 * 
	 * @return Integer
	 * @throws DatabaseException
	 */
	public Integer nextId() throws DatabaseException {
		Integer nextId = 1;
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT MAX(idrequest) FROM request");
			statement.executeQuery();

			ResultSet rs = statement.getResultSet();
			while (rs.next()) {
				nextId = rs.getInt(1) + 1;
			}
		} catch (Exception e) {

			throw new DatabaseException(e.getMessage());
		}
		return nextId;
	}

	/** used to add a comment to a request, inserts the comment in the comment table
	 * @param request
	 * @param comment
	 * @throws DatabaseException
	 */
	public void addCommentToRequest(Request request, Comment comment) throws DatabaseException {
		try {
			PreparedStatement statement = connection
					.prepareStatement("INSERT INTO comment (author, comment, requestid)" + "VALUES(?,?,?)");

			statement.setString(1, comment.getAuthor().getUsername());
			statement.setString(2, comment.getComment());
			statement.setInt(3, request.getId());

			statement.executeUpdate();
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
	}

	/**
	 * used to fetch all comments related to a specified request
	 * @param requestId
	 * @return ArrayList<Comment>
	 * @throws DatabaseException
	 */
	public ArrayList<Comment> fetchCommentsByRequestId(int requestId) throws DatabaseException {
		ArrayList<Comment> comments = new ArrayList<Comment>();
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM comment WHERE requestid = ?");
			statement.setInt(1, requestId);

			statement.executeQuery();

			ResultSet rs = statement.getResultSet();
			while (rs.next()) {
				Comment comment = new Comment();
				comment.setId(rs.getInt(1));
				comment.setAuthor(UserDAO.getInstance().fetchUserByUsername(rs.getString(2)));
				comment.setComment(rs.getString(3));

				comments.add(comment);
			}
		} catch (Exception e) {

			throw new DatabaseException(e.getMessage());
		}
		return comments;
	}

	/**
	 * 
	 * updates  information regarding a request, does not update the requester or assignee
	 * @param request
	 * @throws DatabaseException
	 */
	public void updateRequest(Request request) throws DatabaseException {
		try {
			PreparedStatement statement = connection
					.prepareStatement("UPDATE request " + "SET assigneeid=?, status=?, category=?, subject=?, description=?, approved=? " + "WHERE idrequest=? ");

			String assigneeId = null;
			if(request.getAssignee() != null){
				assigneeId = request.getAssignee().getUsername();
			}
			statement.setString(1, assigneeId);
			statement.setString(2, request.getStatus().getValue());
			statement.setString(3, request.getCategory().getValue());
			statement.setString(4, request.getSubject());
			statement.setString(5, request.getDescription());
			statement.setBoolean(6, request.isApproved());
			statement.setInt(7, request.getId());

			statement.executeUpdate();
			statement.close();
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}		
	}	
	
	
	// for testing purposes
		public void clearTable() throws DatabaseException {
			try {
				PreparedStatement statement = connection.prepareStatement("DELETE FROM request");

				statement.execute();

				statement.close();
			} catch (SQLException e) {
				throw new DatabaseException(e.getMessage());
			}
		}
}
