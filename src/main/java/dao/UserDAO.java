package dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import bean.Administrator;
import bean.Client;
import bean.Request;
import bean.Technician;
import bean.User;
import exceptions.DatabaseException;
import utils.PropertiesLoader;

/**
 * @author Miechko Gibson Wyjadlowski
 *
 */
public class UserDAO {

	private String url;
	private String username;
	private String password;
	private Connection connection;
	private static UserDAO instance = null;

	/**
	 * constructor of the class, feched properties from the external file and
	 * initialises a connection to the database
	 * 
	 * @throws FileNotFoundException
	 * @throws DatabaseException
	 */
	private UserDAO() throws FileNotFoundException, DatabaseException {
		try {
			loadDBProperties();
			connectToDB();
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
	}

	/**
	 * used to start a connection to the database
	 * 
	 * @throws DatabaseException
	 */
	private void connectToDB() throws DatabaseException {
		try {
			connection = DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
	}

	/**
	 * class that load the login credentials and address of the database for the
	 * login
	 * 
	 * @throws IOException
	 */
	private void loadDBProperties() throws IOException {
		Properties prop = PropertiesLoader.loadProperties();

		url = prop.getProperty("dbAdress");
		username = prop.getProperty("dbusername");
		password = prop.getProperty("dbpassword");
	}

	/**
	 * singleton of class
	 * 
	 * @return instance of UserDAO
	 * @throws FileNotFoundException
	 * @throws DatabaseException
	 */
	public static UserDAO getInstance() throws FileNotFoundException, DatabaseException {
		if (instance != null) {
			return instance;
		} else {
			instance = new UserDAO();
			return instance;
		}
	}

	/**
	 * used to fetch the technicians from the database
	 * 
	 * @return ArrayList<Technician>
	 * @throws DatabaseException
	 */
	public ArrayList<Technician> fetchListTechnician() throws DatabaseException {
		ArrayList<Technician> users = new ArrayList<>(100);
		try {
			Statement statement = connection.createStatement();
			statement
					.execute("SELECT * FROM user INNER JOIN technician ON technician.technicianusername=user.username");

			ResultSet rs = statement.getResultSet();
			while (rs.next()) {
				Technician user = new Technician();
				user.setUsername(rs.getString(1));
				user.setFirstname(rs.getString(2));
				user.setLastname(rs.getString(3));
				user.setEmail(rs.getString(4));
				user.setPassword(rs.getString(5));
				user.setPhone(rs.getString(6));
				user.setOffice(rs.getString(7));
				RequestDAO.getInstance().fetchRequestsByUser(user);
				users.add(user);
			}
			statement.close();
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
		return users;
	}

	
	/**
	 * fetches a user and its associated requests, this function is used to prevent circular relationship between user and request
	 * @param username
	 * @return User
	 * @throws Exception
	 */
	public User fetchUserAndRequestsByUsername(String username) throws Exception{
		User user = fetchUserByUsername(username);
		ArrayList<Request> requests = new ArrayList<Request>();
		if(user != null){
			requests = RequestDAO.getInstance().fetchRequestsByUser(user);
			user.setRequests(requests);
		}
		return user;
	}

	/**
	 * fetches a user from the database, using his name
	 * 
	 * @param wantedUsername
	 * @return User
	 * @throws Exception
	 */
	public User fetchUserByUsername(String wantedUsername) throws Exception {
		User user = null;
		try {
			if (userIsClient(wantedUsername)) {
				user = new Client();
			} else if (userIsTechnician(wantedUsername)) {
				user = new Technician();
			} 
			
			if (user != null) {
				PreparedStatement statement = connection.prepareStatement("SELECT * FROM user WHERE username = ? ");
				statement.setString(1, wantedUsername);
				statement.executeQuery();

				ResultSet rs = statement.getResultSet();
				while (rs.next()) {
					user.setUsername(rs.getString(1));
					user.setFirstname(rs.getString(2));
					user.setLastname(rs.getString(3));
					user.setEmail(rs.getString(4));
					user.setPassword(rs.getString(5));
					user.setPhone(rs.getString(6));
					user.setOffice(rs.getString(7));
				}
				statement.close();
			}

		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage());
		}
		return user;
	}

	/**
	 * adds the specified user to the database
	 * 
	 * @param user
	 * @throws DatabaseException
	 */
	public boolean addUser(User user) throws DatabaseException {
		try {
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO user (username, firstname, lastname, email, password, phone, office) "
							+ "VALUES (?, ?, ?, ?, ?, ?, ?)");

			statement.setString(1, user.getUsername());
			statement.setString(2, user.getFirstname());
			statement.setString(3, user.getLastname());
			statement.setString(4, user.getEmail());
			statement.setString(5, user.getPassword());
			statement.setString(6, user.getPhone());
			statement.setString(7, user.getOffice());

			statement.executeUpdate();

			statement.close();

			if (user instanceof Client) {
				addClient(user);
			} else if (user instanceof Technician) {
				addTechnician(user);
				if (user instanceof Administrator) {
					addAdmin(user);
				}
			}

			return true;
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage());
		}

	}

	/**
	 * used to add the user as a client after he is added to the user table (if
	 * he is a client)
	 * 
	 * @param client
	 * @throws DatabaseException
	 */
	private void addClient(User client) throws DatabaseException {
		try {
			PreparedStatement statement = connection
					.prepareStatement("INSERT INTO client (clientusername) " + "VALUES (?)");

			statement.setString(1, client.getUsername());

			statement.executeUpdate();
			statement.close();
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
	}

	/**
	 * used to add the user as a technician after he is added to the user table
	 * (if he is a technician)
	 * 
	 * @param client
	 * @throws DatabaseException
	 */
	private void addAdmin(User admin) throws DatabaseException {
		try {
			PreparedStatement statement = connection
					.prepareStatement("INSERT INTO admin (adminusername) " + "VALUES (?)");

			statement.setString(1, admin.getUsername());

			statement.executeUpdate();
			statement.close();
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
	}

	/**
	 * used to add the user as a technician after he is added to the user table
	 * (if he is a technician)
	 * 
	 * @param client
	 * @throws DatabaseException
	 */
	private void addTechnician(User technician) throws DatabaseException {
		try {
			PreparedStatement statement = connection
					.prepareStatement("INSERT INTO technician (technicianusername) " + "VALUES (?)");

			statement.setString(1, technician.getUsername());

			statement.executeUpdate();
			statement.close();
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
	}

	/**
	 * used to see if a user is a client
	 * 
	 * @param username
	 * @return true if user is a client
	 * @throws DatabaseException
	 */
	private boolean userIsClient(String username) throws DatabaseException {
		try {
			PreparedStatement statement = connection
					.prepareStatement("SELECT count(*) FROM client WHERE clientusername = ?");
			statement.setString(1, username);
			statement.executeQuery();
			ResultSet rs = statement.getResultSet();
			while (rs.next()) {
				if (rs.getInt(1) == 1) {
					return true;
				}
			}
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
		return false;
	}

	/**
	 * used to see if a user is a technician
	 * 
	 * @param username
	 * @return true if user is a technician
	 * @throws DatabaseException
	 */
	private boolean userIsTechnician(String username) throws DatabaseException {
		try {
			PreparedStatement statement = connection
					.prepareStatement("SELECT count(*) FROM technician WHERE technicianusername = ?");
			statement.setString(1, username);
			statement.executeQuery();
			ResultSet rs = statement.getResultSet();
			while (rs.next()) {
				if (rs.getInt(1) == 1) {
					return true;
				}
			}
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		}
		return false;
	}

	
	/**
	 * used to delete a user from the database (admin function)
	 * spaghetti code, should only make the user 'inactive' but lack of time to implement
	 * @param user
	 */
	public void deleteUser(User user){
		try{
			PreparedStatement statement = connection.prepareStatement("DELETE FROM user WHERE username = ?");
			statement.setString(1, user.getUsername());
			statement.executeUpdate();
			statement.close();
		}catch (Exception e) {
		}
	}
	
	/**
	 * used to update values of a user's infos, cannot modify username
	 * @param user
	 */
	public void updateUser(User user){
		try{
			PreparedStatement statement = connection.prepareStatement("UPDATE user"
														+ " SET firstname = ?, lastname = ?, email = ?, password = ?, phone = ?, office = ?"
														+ " WHERE username = ?");
			statement.setString(1, user.getFirstname());
			statement.setString(2, user.getLastname());
			statement.setString(3, user.getEmail());
			statement.setString(4, user.getPassword());
			statement.setString(5, user.getPhone());
			statement.setString(6, user.getOffice());
			statement.setString(7, user.getUsername());

			statement.executeUpdate();
			statement.close();
		}catch (Exception e) {
		}
	}
	// for testing only, removes entries from database
	public void clearTable() throws DatabaseException {
		try {
			PreparedStatement statement = connection.prepareStatement("DELETE FROM user");

			statement.execute();

			statement.close();
		} catch (SQLException e) {
			throw new DatabaseException(e.getMessage());
		}
	}
}