420-504-AL -- TP-Synthèse -- Automne 2016

Énoncé : 
    doc/TP-enonce.docx

Date limite : 
    30 novembre 2016 à 23h55

Équipe : 
   Jessica Barns
   Miechko Gibson Wyjadlowski
   
#Installation de la base de donn�es

1.  Cr�er une base de donn�es mySQL � l'aide du fichier [commandesSQL](./dat/commandesSQL)
2.  Changer l'adresse ip de la variable dbAdress dans le fichier [properties](./properties.properties)
